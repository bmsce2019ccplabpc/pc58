#include<stdio.h>
struct frac
{
 int num[1000];
 int den[1000];
 int n,n1,d1;
};
int gcd(int a,int b)
{
 int i,g;
 for(i=1;i<a&&i<b;i++)
 {
  if(a%i==0&&b%i==0)
  g=i;
 }
 return g;
}
struct frac input()
{
 struct frac p1;
 printf("Enter the number of fractions you want to add\n");
 scanf("%d",&p1.n);
 for(int i=0;i<p1.n;i++)
 {
  printf("Enter the numerator\n");
  scanf("%d",p1.num[i]);
  printf("Enter the denominator\n");
  scanf("%d",p1.den[i]);
 }
 return p1;
}
struct frac compute(struct frac p2)
{
 for(int i=0;i<p2.n;i++)
 {
  int temp=p2.den[++i];
  --i;
  p2.d1=gcd(p2.den[i],p2.den[++i]);
  p2.den[i]=(p2.den[--i]*p2.den[++i])/p2.d1;
  p2.num[i]=(p2.num[--i])*(p2.den[++i]/p2.den[--i]) + (p2.num[++i])*(p2.den[i]/temp);
  --i;
 }
 return p2;
}
void output(struct frac p3)
{
 int temp = p3.n-1;
 printf("The sum of the fractions is %d/%d\n",p3.num[temp],p3.den[temp]);
}
int main()
{
 struct frac p4;
 struct frac p5;
 p4=input();
 p5=compute(p4);
 output(p5);
 return 0;
}
